package thomasrobine.mysecondapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BookActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        final Book book = intent.getExtras().getParcelable("Livre");

        final EditText titre = (EditText) findViewById(R.id.nameBook);
        final EditText auteurs = (EditText) findViewById(R.id.editAuthors);
        final EditText annee = (EditText) findViewById(R.id.editYear);
        final EditText editeur = (EditText) findViewById(R.id.editPublisher);
        final EditText genre = (EditText) findViewById(R.id.editGenres);

        if (book != null) {

            titre.setText(book.getTitle(), TextView.BufferType.EDITABLE);
            auteurs.setText(book.getAuthors(), TextView.BufferType.EDITABLE);
            annee.setText(book.getYear(), TextView.BufferType.EDITABLE);
            editeur.setText(book.getPublisher(), TextView.BufferType.EDITABLE);
            genre.setText(book.getGenres(), TextView.BufferType.EDITABLE);
        }

        Button save = (Button) findViewById(R.id.button);
        assert save !=null;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookDbHelper b = new BookDbHelper(BookActivity.this);
                if (book != null) {
                    book.setTitle(titre.getText().toString());
                    book.setAuthors(auteurs.getText().toString());
                    book.setYear(annee.getText().toString());
                    book.setPublisher(editeur.getText().toString());
                    book.setGenres(genre.getText().toString());
                    book.setId(book.getId());
                    b.updateBook(book);
                }
                else {
                    Book nb = new Book();
                    nb.setTitle(titre.getText().toString());
                    nb.setAuthors(auteurs.getText().toString());
                    nb.setYear(annee.getText().toString());
                    nb.setPublisher(editeur.getText().toString());
                    nb.setGenres(genre.getText().toString());
                    b.addBook(nb);
                }
                Intent nintent = new Intent(BookActivity.this, MainActivity.class);
                startActivity(nintent);
            }
        });

    }


}
